package com.andrei1058.antilockonmyprotection;

import com.griefcraft.model.Permission;
import com.griefcraft.model.Protection;
import com.griefcraft.scripting.JavaModule;
import com.griefcraft.scripting.event.*;
import com.griefcraft.util.UUIDRegistry;
import net.sacredlabyrinth.Phaed.PreciousStones.field.Field;
import net.sacredlabyrinth.Phaed.PreciousStones.field.FieldFlag;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Listeners extends JavaModule {

    @Override
    public void onRegisterProtection(LWCProtectionRegisterEvent e) {
        Field f = Main.plugin.getPs().API().getFieldsProtectingArea(FieldFlag.ALL, e.getBlock().getLocation()).get(0);
        if (f != null) {
            if (!f.getAllowed().contains(e.getPlayer().getName().toLowerCase())) {
                e.setCancelled(true);
                e.getPlayer().sendMessage(Main.config.getMsg(Path.MSG_CANT_LOCK_ON_PROTECTION));
            }

        }
    }

    @Override
    public void onRegisterEntity(LWCProtectionRegisterEntityEvent e) {
        Field f = Main.plugin.getPs().API().getFieldsProtectingArea(FieldFlag.ALL, e.getEntity().getLocation()).get(0);
        if (f != null) {
            if (!f.getAllowed().contains(e.getPlayer().getName().toLowerCase())) {
                e.setCancelled(true);
                e.getPlayer().sendMessage(Main.config.getMsg(Path.MSG_CANT_LOCK_ON_PROTECTION));
            }

        }
    }

    @Override
    public void onPostRegistration(LWCProtectionRegistrationPostEvent e) {
        Protection prot = e.getProtection();
        Player p = e.getPlayer();
        Field f = Main.plugin.getPs().API().getFieldsProtectingArea(FieldFlag.ALL, prot.getBlock().getLocation()).get(0);
        if (f != null) {
            Player owner = Bukkit.getPlayer(f.getOwner());
            if (owner != null) {
                if ((!owner.getName().equals(p.getName())) && Main.config.getBoolean(Path.SETT_AUTO_GIVE_OWNER_ACCESS)) {
                    prot.addPermission(new Permission(UUIDRegistry.getUUID(owner.getName()).toString(), Permission.Type.PLAYER, Permission.Access.PLAYER));
                    prot.save();
                    p.sendMessage(Main.config.getMsg(Path.MSG_ACCESS_GIVEN_TO_OWNER));
                }
            }
            if (Main.config.getBoolean(Path.SETT_AUTO_GIVE_MEMBERS_ACCESS)) {
                for (String member : f.getAllowed()) {
                    if (member.equalsIgnoreCase(p.getName())) continue;
                    Player m = Bukkit.getPlayer(member);
                    if (m != null) {
                        prot.addPermission(new Permission(UUIDRegistry.getUUID(m.getName()).toString(), Permission.Type.PLAYER, Permission.Access.PLAYER));
                    }
                    prot.save();
                    p.sendMessage(Main.config.getMsg(Path.MSG_ACCESS_GIVEN_TO_FIELD_MEMBERS));
                }
            }
        }
    }

    @Override
    public void onDestroyProtection(LWCProtectionDestroyEvent e) {
        if (Main.config.getBoolean(Path.SETT_ALLOW_FIELD_OWNER_TO_BREAK)){
            Field f = Main.plugin.getPs().API().getFieldsProtectingArea(FieldFlag.ALL, e.getProtection().getBlock().getLocation()).get(0);
            if (f != null) {
                Player owner = Bukkit.getPlayer(f.getOwner());
                if (owner != null) {
                    if (Bukkit.getOnlineMode()) {
                        if (e.getPlayer().getUniqueId().equals(owner.getUniqueId())) {
                            e.setCancelled(false);
                        }
                    } else {
                        if (e.getPlayer().getName().equals(owner.getName())) {
                            e.setCancelled(false);
                        }
                    }
                }
            }
        }
    }
}
