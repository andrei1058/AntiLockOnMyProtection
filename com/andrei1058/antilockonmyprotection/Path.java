package com.andrei1058.antilockonmyprotection;

public class Path {
    public static String MSG_CANT_LOCK_ON_PROTECTION = "messages.cantLock";
    public static String MSG_ACCESS_GIVEN_TO_OWNER = "messages.ownerAccess";
    public static String MSG_ACCESS_GIVEN_TO_FIELD_MEMBERS = "messages.membersAccess";

    public static String SETT_AUTO_GIVE_OWNER_ACCESS = "settings.autoGiveAccessToFieldOwner";
    public static String SETT_AUTO_GIVE_MEMBERS_ACCESS = "settings.autoGiveAccessToFieldMembers";
    public static String SETT_ALLOW_FIELD_OWNER_TO_BREAK = "settings.fieldOwnerCanBreakOthersChests";
}
