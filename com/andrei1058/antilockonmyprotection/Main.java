package com.andrei1058.antilockonmyprotection;

import com.griefcraft.lwc.LWC;
import com.griefcraft.lwc.LWCPlugin;
import net.sacredlabyrinth.Phaed.PreciousStones.PreciousStones;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {

    private static LWC lwc;
    private static PreciousStones ps;
    public static Main plugin;
    public static Config config;

    @Override
    public void onEnable() {
        plugin = this;
        if (!(this.getServer().getPluginManager().isPluginEnabled("PreciousStones"))) {
            getLogger().severe("Couldn't find PreciousStones, please install PreciousStones before running " + getName());
            this.getPluginLoader().disablePlugin(this);
            return;
        }
        if (!(this.getServer().getPluginManager().isPluginEnabled("LWC"))) {
            getLogger().severe("Couldn't find LWC, please install LWC before running " + getName());
            this.getPluginLoader().disablePlugin(this);
            return;
        }
        Plugin lwcp = Bukkit.getPluginManager().getPlugin("LWC");
        lwc = ((LWCPlugin) lwcp).getLWC();
        Plugin psp = Bukkit.getPluginManager().getPlugin("PreciousStones");
        ps = ((PreciousStones) psp).getInstance();
        lwc.getModuleLoader().registerModule(this, new Listeners());
        setupConfig();
    }

    /**
     * Get LWC instance
     */
    public static LWC getLwc() {
        return lwc;
    }

    /**
     * Get PreciousStones instance
     **/
    public static PreciousStones getPs() {
        return ps;
    }

    /** Setup configuration*/
    public static void setupConfig(){
        config = new Config("config", "plugins/"+plugin.getName());
        YamlConfiguration yml = config.getYml();
        yml.options().copyDefaults(true);
        yml.addDefault(Path.SETT_ALLOW_FIELD_OWNER_TO_BREAK, true);
        yml.addDefault(Path.SETT_AUTO_GIVE_OWNER_ACCESS, true);
        yml.addDefault(Path.SETT_AUTO_GIVE_MEMBERS_ACCESS, false);
        yml.addDefault(Path.MSG_CANT_LOCK_ON_PROTECTION, "&8Info> &fYou can't do that because you don't have access to this protection field.");
        yml.addDefault(Path.MSG_ACCESS_GIVEN_TO_OWNER, "&8Info> &fThe field's owner has automatically received access to this object.");
        yml.addDefault(Path.MSG_ACCESS_GIVEN_TO_FIELD_MEMBERS, "&8Info> &fThe members of this protection field have automatically received access to this object..");
        config.save();
    }
}
