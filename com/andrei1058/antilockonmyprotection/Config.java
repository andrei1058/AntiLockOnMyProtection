package com.andrei1058.antilockonmyprotection;

import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class Config {

    private YamlConfiguration yml;
    private File config;

    public Config(String name, String dir){
        File d = new File(dir);
        if (!d.exists()){
            d.mkdir();
        }
        config = new File(dir+"/"+name+".yml");
        if (!config.exists()){
            try {
                config.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Main.plugin.getLogger().info("Creating "+dir+"/"+name+".yml");
        }
        yml = YamlConfiguration.loadConfiguration(config);
    }

    public void reload(){
        yml = YamlConfiguration.loadConfiguration(config);
    }

    public void set(String path, Object value){
        yml.set(path, value);
        save();
    }

    public String getMsg(String path){
        return yml.getString(path).replace("&", "§");
    }

    public YamlConfiguration getYml() {
        return yml;
    }

    public void save(){
        try {
            yml.save(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean getBoolean(String path){
        return yml.getBoolean(path);
    }
}
